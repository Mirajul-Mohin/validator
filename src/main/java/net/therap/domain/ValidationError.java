package net.therap.domain;

/**
 * @author mirajul.mohin
 * @since 2/6/20
 */
public class ValidationError {

    private String error;

    public ValidationError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
