package net.therap.app;

import net.therap.domain.Person;
import net.therap.domain.ValidationError;
import net.therap.service.AnnotatedValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author mirajul.mohin
 * @since 2/5/20
 */
public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        int age = sc.nextInt();

        Person p = new Person(name, age);

        List<ValidationError> errors = new ArrayList<ValidationError>();

        AnnotatedValidator validator = new AnnotatedValidator();
        validator.validate(p, errors);
        validator.print(errors);
    }
}
